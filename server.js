const faker = require('faker')
const express = require('express')
const app = express()
const db = require('./db')
const { User } = require('./models/user')

db.connect()

app.get('/', (req, res) => {   
    res.send({ message: 'Hello World!' }) 
})   

app.get('/users', async (req, res) => {   
    const users = await User.find()
    res.send(users) 
})   

app.get('/users/create', async (req, res) => {   
    let user = new User({
        name: `${faker.name.lastName()} ${faker.name.firstName()}`,
        email: faker.internet.email(),
    })
    user = await user.save()
    res.send(user) 
})   

const port = process.env.PORT || 3000
const server = app.listen(port, function () {   
    console.log('Example app listening on port 3000!') 
})   
    
module.exports = server 
