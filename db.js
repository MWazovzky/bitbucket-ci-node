const config = require('config')
const mongoose = require('mongoose')

const connect = () => {
    mongoose.connect(config.get('mongodb-uri'), { useNewUrlParser: true }) 
        .then(() => console.log('Connected to MongoDB...'))
        .catch((err) => console.log('Connection failed. Error: ' + err.message))
}

module.exports = { connect }