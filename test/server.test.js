const request = require('supertest')

describe('GET /', function() {  
    let server

    beforeEach(() => {
        server = require('../server.js')
    })

    afterEach(async () => {
        await server.close()
    })

    it('displays "Hello World!"', async () => {     
        const res = await request(server).get('/')
        expect(res.body).toHaveProperty('message', 'Hello World!')   
    })
})
